# junit-when

This project bridges JUnit and Mockito to [When.java](https://github.com/ef-labs/when.java), making it possible 
to write asynchronous unit tests. Here's the Maven dependency:

```
<dependency>
    <groupId>com.phenixidentity</groupId>
    <artifactId>junit-when</artifactId>
    <version>0.3</version>
</dependency>
```


#### Example

Suppose that you have an event bus, that asynchronously sends and receives messages, using `Promise`s 
to handle future resolutions or rejects:

```java
    public Promise<String> doSomethingAsync() {
        Promise<String> result = bus.send("First request")
                .then(response -> {
                    return bus.send("Second request");
                }).then(response -> {
                    return when.resolve("Success!");
                }).otherwise(IOException.class, response -> {
                    return when.resolve("Fail!");
                });
        return result;
    }
```

Using `junit-when` and Mockito, you can easily mock the event bus and write a simple unit test for the above method like this:

```java
    @Test
    public Promise<String> test() {
          // set up test subject with eventBusMock
          MyTestSubject testSubject = ...;
    
          // set up assertions
          asserts.with(result -> Assert.assertEquals("Success!", result))
               .with(result -> Mockito.verify(evenBusMock, times(3)).send(anyString()));
          
          // exercise test subject
          Promise<String> result = testSubject.doSomethingAsync();
          
          // resolve async responses
          queue.resolveNext("First response");
          queue.resolveNext("Second response");
          
          return result;
    }
```

or you could set up the test for an expected failure when the `send` method is called for the second time:

```java
    @Test
    public Promise<String> test() {
          // set up test subject with eventBusMock
          MyTestSubject testSubject = ...;
    
          // set up assertion
          expectedException.expect(NullPointerException.class);
          
          // exercise test subject
          Promise<String> result = testSubject.doSomethingAsync();
          
          // resolve async responses
          queue.resolveNext("First response");
          queue.rejectNext(new NullPointerException());
          
          return result;
    }
```

For details, please have a look at the demo in the `test` directory.