package com.phenixidentity.junit.runners;

import com.englishtown.promises.*;
import com.phenixidentity.junit.internal.runners.statements.AsyncInvokeMethod;
import com.phenixidentity.junit.rules.AsyncRunRules;
import com.phenixidentity.junit.rules.AsyncTestRule;
import com.phenixidentity.junit.rules.AsyncVerifier;
import com.phenixidentity.junit.runners.model.AsyncStatement;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.internal.AssumptionViolatedException;
import org.junit.internal.runners.model.EachTestNotifier;
import org.junit.internal.runners.model.ReflectiveCallable;
import org.junit.rules.RunRules;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;

import java.lang.annotation.Annotation;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Use this test runner to run unit tests for asynchronous code based on <a target="_blank" href="https://github.com/ef-labs/when.java">{@code When.java}</a>.
 * <p>
 * Each test must return a {@code Promise<T>} of some type {@code T}. The value that the promise resolves to, or the
 * exception with which it rejects,
 * can then be inspected and asserted with the help of {@code org.junit.Rule}s with support for {@code Promise}, e.g.
 * {@code AsyncVerifier} and/or {@code AsyncExpectedException}.
 * <p>
 * Because of the asynchronous nature of the tests, where one {@code Promise} might wait indefinitely for another
 * to resolve or reject, there is a timeout specified by the system property {@code com.phenixidentity.junit.runners.timeout}
 * (default value 10 seconds). To disable the timeout, set the value to anything less than zero.
 */
public class WhenTestRunner extends BlockJUnit4ClassRunner {

    private final Class testClass;

    private final When when = WhenFactory.createSync();

    public WhenTestRunner(Class<?> testClass) throws InitializationError {
        super(testClass);
        this.testClass = testClass;
    }

    @Override
    public Description getDescription() {
        return Description.createTestDescription(testClass, "Test runner with support for When.java Promises/A+");
    }

    @Override
    protected void runChild(final FrameworkMethod method, RunNotifier notifier) {
        Description description = describeChild(method);
        if (method.getAnnotation(Ignore.class) != null) {
            notifier.fireTestIgnored(description);
        } else {
            runAsyncLeaf(methodBlock(method), description, notifier);
        }
    }

    protected AsyncStatement methodBlock(FrameworkMethod method) {
        Deferred<?> testResult = when.defer();
        Object test;
        try {
            test = new ReflectiveCallable() {
                @Override
                protected Object runReflectiveCall() throws Throwable {
                    return createTest();
                }
            }.run();
        } catch (Throwable e) {
            return new AsyncStatement() {
                @Override
                public Promise<Void> getPromise() {
                    return null;
                }

                @Override
                public void evaluate() throws Throwable {
                    throw e;
                }
            };
        }
        Statement statement = new AsyncInvokeMethod<>(method, test, testResult);
        statement = withRules(method, test, statement, testResult);
        return (AsyncStatement) statement;
    }

    @Override
    protected void validateTestMethods(List<Throwable> errors) {
        this.validatePublicPromiseNoArgMethods(Test.class, errors);
    }

    protected final void runAsyncLeaf(AsyncStatement statement, Description description, RunNotifier notifier) {
        long timeout = Long.parseLong(System.getProperty("com.phenixidentity.junit.runners.timeout", "10000"));
        EachTestNotifier eachNotifier = new EachTestNotifier(notifier, description);
        eachNotifier.fireTestStarted();
        try {
            long start = System.currentTimeMillis();
            statement.evaluate();
            Promise<?> finishTest = statement.getPromise();
            finishTest.then(v -> {
                synchronized (finishTest) {
                    finishTest.notify();
                }
                return when.resolve(v);
            }).otherwise(cause -> {
                synchronized (finishTest) {
                    finishTest.notify();
                }
                return when.reject(cause);
            });
            synchronized (finishTest) {
                if (finishTest.inspect().getState() == HandlerState.PENDING) {
                    if (timeout < 0) {
                        finishTest.wait();
                    } else {
                        finishTest.wait(timeout);
                    }
                }
            }
            long duration = System.currentTimeMillis() - start;
            if (finishTest.inspect().getState() == HandlerState.REJECTED) {
                throw finishTest.inspect().getReason();
            } else if (finishTest.inspect().getState() == HandlerState.PENDING) {
                final var allStackTraces = Thread.getAllStackTraces();
                for(var entry : allStackTraces.entrySet()) {
                    final var thread = entry.getKey();
                    final var stackTraceEntries = entry.getValue();
                    System.out.println("Thread: " + thread.getName() + " [" + thread.getId() + "]: " + thread.getState());
                    for(var stackEntry : stackTraceEntries) {
                        System.out.println("  " + stackEntry.toString());
                    }
                }
                throw new IllegalStateException("Timed out before reaching FULFILLED state (duration: " + duration + " ms, timeout: " + timeout + " ms).\nChange timeout period by setting the system property \'com.phenixidentity.junit.runners.timeout\'. A value less than 0 means that the tests will never timeout.");
            }
        } catch (AssumptionViolatedException e) {
            eachNotifier.addFailedAssumption(e);
        } catch (Throwable e) {
            eachNotifier.addFailure(e);
        } finally {
            eachNotifier.fireTestFinished();
        }
    }

    private AsyncStatement withRules(FrameworkMethod method, Object target, Statement statement, Deferred<?> deferredResult) {
        List<TestRule> allRules = getTestRules(target);
        List<TestRule> testRules = allRules.stream()
                .filter(rule -> !(rule instanceof AsyncTestRule))
                .collect(Collectors.toList());
        List<AsyncTestRule> asyncTestRules = allRules.stream()
                .filter(rule -> rule instanceof AsyncTestRule)
                .map(AsyncTestRule.class::cast)
                .sorted((rule1, rule2) -> { // Verify rules must execute first
                    if (rule1.getClass().equals(rule2.getClass())) {
                        return 0;
                    }
                    if (AsyncVerifier.class.isAssignableFrom(rule1.getClass())) {
                        return 1;
                    }
                    return -1;
                })
                .collect(Collectors.toList());

        Statement result = statement;
        result = this.withBefores(method, target, result);
        result = this.withAfters(method, target, result);
        result = withMethodRules(method, testRules, target, result);
        result = withTestRules(method, testRules, result);
        result = withWhenTestRules(method, asyncTestRules, result, deferredResult);

        return (AsyncStatement) result;
    }

    private Statement withMethodRules(FrameworkMethod method, List<TestRule> testRules, Object target, Statement result) {
        for (org.junit.rules.MethodRule each : rules(target)) {
            if (!testRules.contains(each)) {
                result = each.apply(result, method, target);
            }
        }
        return result;
    }

    private Statement withTestRules(FrameworkMethod method, List<TestRule> testRules, Statement statement) {
        return testRules.isEmpty() ? statement :
                new RunRules(statement, testRules, describeChild(method));
    }

    private AsyncStatement withWhenTestRules(FrameworkMethod method, List<AsyncTestRule> asyncTestRules, Statement statement, Deferred<?> deferredResult) {
        return new AsyncRunRules(statement, asyncTestRules, describeChild(method), deferredResult);
    }

    protected void validatePublicPromiseNoArgMethods(Class<? extends Annotation> annotation, List<Throwable> errors) {
        List<FrameworkMethod> methods = getTestClass().getAnnotatedMethods(annotation);

        for (FrameworkMethod eachTestMethod : methods) {
            validatePublicPromise(eachTestMethod, false, errors);
            if (eachTestMethod.getMethod().getParameterTypes().length != 0) {
                errors.add(new Exception("Method " + eachTestMethod.getName() + " should have no parameters"));
            }
        }
    }

    public void validatePublicPromise(FrameworkMethod fMethod, boolean isStatic, List<Throwable> errors) {
        if (Modifier.isStatic(fMethod.getMethod().getModifiers()) != isStatic) {
            String state = isStatic ? "should" : "should not";
            errors.add(new Exception("Method " + fMethod.getName() + "() " + state + " be static"));
        }
        if (!Modifier.isPublic(fMethod.getMethod().getDeclaringClass().getModifiers())) {
            errors.add(new Exception("Class " + fMethod.getMethod().getDeclaringClass().getName() + " should be public"));
        }
        if (!Modifier.isPublic(fMethod.getMethod().getModifiers())) {
            errors.add(new Exception("Method " + fMethod.getName() + "() should be public"));
        }
        if (!Promise.class.isAssignableFrom(fMethod.getReturnType())) {
            errors.add(new Exception("Method " + fMethod.getName() + "() should have return type " + Promise.class.getName()));
        }
    }
}
