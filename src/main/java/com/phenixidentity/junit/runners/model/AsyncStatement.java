package com.phenixidentity.junit.runners.model;

import com.englishtown.promises.Promise;
import org.junit.runners.model.Statement;

public abstract class AsyncStatement extends Statement {
    public abstract Promise<?> getPromise();
}
