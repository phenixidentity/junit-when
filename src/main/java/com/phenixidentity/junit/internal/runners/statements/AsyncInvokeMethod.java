package com.phenixidentity.junit.internal.runners.statements;


import com.englishtown.promises.Deferred;
import com.englishtown.promises.Promise;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

public class AsyncInvokeMethod<T> extends Statement {
    private final FrameworkMethod fTestMethod;
    private Object fTarget;
    private final Deferred<T> result;

    public AsyncInvokeMethod(FrameworkMethod testMethod, Object target, Deferred<T> deferred) {
        fTestMethod = testMethod;
        fTarget = target;
        this.result = deferred;
    }

    @Override
    public void evaluate() throws Throwable {
        // noinspection unchecked
        result.resolve(((Promise<T>) fTestMethod.invokeExplosively(fTarget)));
    }
}
