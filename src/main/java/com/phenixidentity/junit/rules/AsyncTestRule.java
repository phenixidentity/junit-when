package com.phenixidentity.junit.rules;

import com.englishtown.promises.Promise;
import org.junit.rules.TestRule;

public interface AsyncTestRule<T> extends TestRule {

    void setCurrent(Promise<T> promise);

    Promise<T> getNext();
}
