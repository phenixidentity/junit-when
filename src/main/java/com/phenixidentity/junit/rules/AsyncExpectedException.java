package com.phenixidentity.junit.rules;

import com.englishtown.promises.Promise;
import com.englishtown.promises.When;
import com.englishtown.promises.WhenFactory;
import org.junit.Assert;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The {@code AsyncExpectedException} allows you to verify that the {@code Promise} returned by the code under test
 * rejects with a specific exception.
 */
public class AsyncExpectedException implements AsyncTestRule {

    private final Set<Class<? extends Throwable>> types = new HashSet<>();
    private final When when = WhenFactory.createSync();
    private Promise<?> next;

    /**
     * Verify that the {@code Promise} returned by the code under test rejects with a reason that is an
     * instance of specific {@code type}.
     * <pre> &#064;Test
     * public void throwsExceptionWithSpecificType() {
     *     thrown.expect(NullPointerException.class);
     *     throw new NullPointerException();
     * }</pre>
     *
     * @param type the expected exception type
     * @return {@code this}
     */
    public AsyncExpectedException expect(Class<? extends Throwable> type) {
        types.add(type);
        return this;
    }

    @Override
    public Statement apply(Statement base, Description description) {
        next = next
                .then(v -> {
                    if (types.size() > 0)
                        Assert.fail("Expected exception did not occur: " + types.stream().map(Class::getName).collect(Collectors.joining(",")));
                    return when.resolve(v);
                })
                .otherwise(throwable -> types.contains(throwable.getClass()), when::resolve)
                .otherwise(cause -> {
                    Assert.fail(cause.getClass().getName() + ": " + cause.getMessage());
                    return null;
                });
        return base;
    }

    @Override
    public void setCurrent(Promise promise) {
        next = promise;
    }

    @Override
    public Promise getNext() {
        return next;
    }
}
