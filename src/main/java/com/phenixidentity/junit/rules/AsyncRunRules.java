package com.phenixidentity.junit.rules;

import com.englishtown.promises.Deferred;
import com.englishtown.promises.Promise;
import com.phenixidentity.junit.runners.model.AsyncStatement;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

public class AsyncRunRules extends AsyncStatement {
    private final Statement statement;
    private Promise next;

    public AsyncRunRules(Statement base, Iterable<AsyncTestRule> rules, Description description, Deferred<?> deferredResult) {
        statement = applyAll(base, rules, description, deferredResult);
    }

    @Override
    public void evaluate() throws Throwable {
        statement.evaluate();
    }

    @Override
    public Promise<?> getPromise() {
        return next;
    }

    private Statement applyAll(Statement result, Iterable<AsyncTestRule> rules,
                               Description description, Deferred<?> deferredResult) {
        next = deferredResult.getPromise();
        for (AsyncTestRule each : rules) {
            each.setCurrent(next);
            result = each.apply(result, description);
            next = each.getNext();
        }
        return result;
    }
}
