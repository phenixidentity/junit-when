package com.phenixidentity.junit.rules;

import com.englishtown.promises.Promise;
import com.englishtown.promises.When;
import com.englishtown.promises.WhenFactory;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * The {@code AsyncVerifier} allows you to verify the resolved value or reject reason from your test subject.
 *
 * @param <T> the expected return type
 */
public class AsyncVerifier<T> implements AsyncTestRule<T> {

    private final When when = WhenFactory.createSync();
    private Promise<T> next;
    private final List<Consumer<T>> asserts = new ArrayList<>();

    @Override
    public Statement apply(Statement base, Description description) {
        next = next.then(value -> {
            applyAll(value);
            return when.resolve(value);
        });
        return base;
    }

    @Override
    public void setCurrent(Promise<T> promise) {
        next = promise;
    }

    @Override
    public Promise<T> getNext() {
        return next;
    }

    /**
     * Add a consumer of type {@code T}, that asserts the response value or reject reason.
     * It is possible to add multiple such consumers.
     * <pre> &#064;Test
     * public void returnsString() {
     *     asserts.with(s -&#062; Assert.notNull(s)).with(s -&#062; Assert.assertEquals("the result", s));
     *     return when.resolve("the result");
     * }</pre>
     *
     * @param verifier a consumer that asserts the correct return value.
     * @return {@code this}
     */
    public AsyncVerifier<T> with(Consumer<T> verifier) {
        asserts.add(verifier);
        return this;
    }

    private void applyAll(T value) {
        asserts.forEach(c -> c.accept(value));
    }
}
