package com.phenixidentity.junit.promises;


import com.englishtown.promises.Deferred;
import com.englishtown.promises.Promise;

import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 * This class lets the client enqueue {@code Deferred} objects, then resolve or reject
 * the enqueued items with given values, in a FIFO order.
 * <p>
 * The intended usage is together with Mockito. Say one wishes to mock the interface {@code AsyncProvider},
 * with the single method {@code Promise<String> send(String message)}. Each call to the mocked {@code send} method
 * should enqueue a new {@code Deferred}, so that it can later be resolved or rejected with the value of choice:
 * <pre>
 * {@code
 * PromiseQueue queue = new PromiseQueue<>();
 * AsyncProvider asyncProvider = mock(AsyncProvider.class);
 * when(asyncProvider.send(anyString()))
 * .thenAnswer(invocation -> queue.enqueue(when.defer()));
 * }
 * </pre>
 * <p>
 * Now, by calling {@code queue.resolveNext("Some response")}, the {@code Promise} returned by the first call to
 * {@code send} will resolve to the value "Some response". Likewise, the call
 * {@code queue.rejectNext(new RuntimeException()} would cause the {@code Promise} to reject with the given exception.
 */
public class PromiseQueue<T> {

    private final LinkedList<Deferred<T>> queue = new LinkedList<>();

    /**
     * Enqueue a {@code Deferred} object, so that it can later be resolved or rejected with a given value
     *
     * @param deferred a deferred object of type {@code T}
     * @return the promise corresponding to the given {@code Deferred}
     */
    public Promise<T> enqueue(Deferred<T> deferred) {
        synchronized (queue) {
            queue.addLast(deferred);
            queue.notifyAll();
        }
        return deferred.getPromise();
    }

    /**
     * Resolve the {@code Deferred} first in queue. If no item is found in the queue, wait indefinitely for an item to be enqueued.
     *
     * @param response the resolved value
     * @throws InterruptedException  when the waiting thread is interrupted before the enqueued item was de-queued
     * @throws IllegalStateException when a timeout occurs while waiting for an item to be enqueued
     */
    public void resolveNext(T response) throws InterruptedException, IllegalStateException {
        synchronized (queue) {
            while (queue.isEmpty()) {
                try {
                    queue.wait();
                }
                catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }
        Deferred<T> deferred;
        synchronized (queue) {
            try {
                deferred = queue.removeFirst();
            } catch (NoSuchElementException e) {
                throw new IllegalStateException("Timed out waiting for enqueued item. This probably means that the code under test was not called as per expectation");
            }
        }
        deferred.resolve(response);
    }

    /**
     * Resolve the {@code Deferred} first in queue. If no item is found in the queue, wait {@code timeout} millis for an item to be enqueued.
     *
     * @param response the resolved value
     * @param timeout  timeout after which the wait will be interrupted
     * @throws InterruptedException  when the waiting thread is interrupted before the enqueued item was de-queued
     * @throws IllegalStateException when a timeout occurs while waiting for an item to be enqueued
     */
    public void resolveNext(T response, long timeout) throws InterruptedException, IllegalStateException {
        System.out.println("resolveNext: Begin");
        final var startTimeNano = System.nanoTime();
        final var timeoutThresholdNano = startTimeNano + timeout * 1_000_000;
        synchronized (queue) {
            while (queue.isEmpty() && System.nanoTime() < timeoutThresholdNano) {
                try {
                    System.out.println("resolveNext: Waiting");
                    queue.wait(timeout);
                }
                catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
                System.out.println("resolveNext: Waited");
            }
        }
        Deferred<T> deferred;
        synchronized (queue) {
            try {
                System.out.println("resolveNext: Dequeueing");
                deferred = queue.removeFirst();
            } catch (NoSuchElementException e) {
                System.out.println("resolveNext: Failed");
                throw new IllegalStateException("Timed out waiting for enqueued item. This probably means that the code under test was not called as per expectation within the timeout period (" + timeout + " ms)");
            }
            System.out.println("resolveNext: Resolving");
        }
        deferred.resolve(response);
        System.out.println("resolveNext: Resolved");
    }

    /**
     * Reject the {@code Deferred} first in queue. If no item is found in the queue, wait {@code timeout} millis for an item to be enqueued.
     *
     * @param throwable the reject reason
     * @param timeout   timeout after which the wait will be interrupted
     * @throws InterruptedException  when the waiting thread is interrupted before the enqueued item was de-queued
     * @throws IllegalStateException when a timeout occurs while waiting for an item to be enqueued
     */
    public void rejectNext(Throwable throwable, long timeout) throws InterruptedException, IllegalStateException {
        System.out.println("rejectNext: Begin");
        final var startTimeNano = System.nanoTime();
        final var timeoutThresholdNano = startTimeNano + timeout * 1_000_000;
        synchronized (queue) {
            while (queue.isEmpty() && System.nanoTime() < timeoutThresholdNano) {
                try {
                    System.out.println("rejectNext: Waiting");
                    queue.wait(timeout);
                }
                catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
                System.out.println("rejectNext: Waited");
            }
        }
        Deferred deferred;
        synchronized (queue) {
            try {
                System.out.println("rejectNext: Dequeueing");
                deferred = queue.removeFirst();
            } catch (NoSuchElementException e) {
                System.out.println("rejectNext: Failed");
                throw new IllegalStateException("Timed out waiting for enqueued item. This probably means that the code under test was not called as per expectation within the timeout period (" + timeout + " ms)");
            }
            System.out.println("rejectNext: Resolving");
        }
        deferred.reject(throwable);
        System.out.println("rejectNext: Resolved");
    }

    /**
     * Reject the {@code Deferred} first in queue. If no item is found in the queue, wait indefinitely for an item to be enqueued.
     *
     * @param throwable the reject reason
     * @throws InterruptedException  when the waiting thread is interrupted before the enqueued item was de-queued
     * @throws IllegalStateException when a timeout occurs while waiting for an item to be enqueued
     */
    public void rejectNext(Throwable throwable) throws InterruptedException, IllegalStateException {
        synchronized (queue) {
            while (queue.isEmpty()) {
                try {
                    queue.wait();
                }
                catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }
        Deferred deferred;
        synchronized (queue) {
            try {
                deferred = queue.removeFirst();
            } catch (NoSuchElementException e) {
                System.out.println("rejectNext: no timeout: Failed: " + e.getMessage());
                throw new IllegalStateException("Timed out waiting for enqueued item. This probably means that the code under test was not called as per expectation");
            }
        }
        deferred.reject(throwable);
        deferred.getPromise();
    }

    public int peekCount() {
        synchronized (this.queue) {
            return this.queue.size();
        }
    }
}
