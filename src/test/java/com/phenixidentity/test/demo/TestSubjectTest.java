package com.phenixidentity.test.demo;

import com.englishtown.promises.Promise;
import com.phenixidentity.junit.rules.AsyncExpectedException;
import com.phenixidentity.junit.rules.AsyncVerifier;
import com.phenixidentity.junit.runners.WhenTestRunner;
import com.phenixidentity.test.util.TestResource;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * <i>This class is part of a demonstration of how to set up unit tests for a test subject that is using an asynchronous, <a target="_blank" href="https://github.com/ef-labs/when.java">{@code When.java}</a> based API</i>
 */
@RunWith(WhenTestRunner.class)
public class TestSubjectTest {

    @Rule
    public TestResource testResource = new TestResource(false);

    @Rule
    public AsyncExpectedException expectedException = new AsyncExpectedException();

    @Rule
    public AsyncVerifier<String> responseVerifier = new AsyncVerifier<>();

    @Rule
    public AsyncVerifier<RuntimeException> exceptionVerifier = new AsyncVerifier<>();

    @Test
    public Promise<String> shouldRespondSuccess() throws InterruptedException {

        // set up test subject
        TestSubject testSubject = new TestSubject(testResource.getMock());

        // set up asserts
        responseVerifier.with(response -> Assert.assertEquals("all good", response));

        // exercise test subject
        Promise<String> send = testSubject.doSomethingAsync("first message");
        testResource.resolveNext("first response");
        testResource.resolveNext("second response");
        testResource.resolveNext("all good");

        return send;
    }

    @Test
    public Promise<String> shouldThrowIllegalArgumentException() throws InterruptedException {

        // set up test subject
        TestSubject testSubject = new TestSubject(testResource.getMock());

        // set up asserts
        expectedException.expect(IllegalArgumentException.class);

        // exercise test subject
        Promise<String> send = testSubject.doSomethingAsync("first message");
        testResource.resolveNext("first response");
        testResource.resolveNext("second response");
        testResource.rejectNext(new IllegalArgumentException("Could not send message!"));

        return send;
    }

    @Test
    public Promise<String> shouldThrowWithCannotHandleResponseMessage() throws InterruptedException {

        // set up test subject
        TestSubject testSubject = new TestSubject(testResource.getMock());

        // set up asserts
        expectedException.expect(RuntimeException.class);
        exceptionVerifier.with(response -> Assert.assertTrue(response.getMessage().startsWith("Cannot handle response:")));

        // exercise test subject
        Promise<String> send = testSubject.doSomethingAsync("first message");
        testResource.resolveNext("first response");
        testResource.resolveNext("unknown response");

        return send;
    }
}
