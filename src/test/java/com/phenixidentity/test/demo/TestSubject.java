package com.phenixidentity.test.demo;

import com.englishtown.promises.Promise;
import com.example.EventBus;

/**
 * <i>This class is part of a demonstration of how to set up unit tests for a test subject that is using an asynchronous, <a target="_blank" href="https://github.com/ef-labs/when.java">{@code When.java}</a> based API</i>
 * <p>
 * <p>
 * This is the class under test in the demo. The class is a client of the {@code EventBus} API. It is intended to demonstrate
 * how an async test might be designed.
 * <p>
 * A call to {@code doSomethingAsync} will generate a series of calls to
 * {@code EventBus.send(String message)}. The returned {@code Promise} represents the final result of the calls.
 */
public class TestSubject {
    private final EventBus eventBus;

    public TestSubject(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    public Promise<String> doSomethingAsync(String input) {
        return eventBus.send(input)
                .then(this::handleResponse)
                .then(this::handleResponse)
                .otherwise(IllegalArgumentException.class, throwable -> {
                    throw (IllegalArgumentException) throwable;
                });
    }

    private Promise<String> handleResponse(String response) {
        System.out.println("Response received: " + response);
        switch (response) {
            case "first response":
                return eventBus.send("some message");
            case "second response":
                return eventBus.send("some other message");
        }
        throw new RuntimeException("Cannot handle response: " + response);
    }
}
