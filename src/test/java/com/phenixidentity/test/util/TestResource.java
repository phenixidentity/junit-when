package com.phenixidentity.test.util;

import com.englishtown.promises.When;
import com.englishtown.promises.WhenFactory;
import com.example.EventBus;
import com.phenixidentity.junit.promises.PromiseQueue;
import org.junit.rules.ExternalResource;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * <i>This class is part of a demonstration of how to set up unit tests for a test subject that is using an asynchronous, <a target="_blank" href="https://github.com/ef-labs/when.java">{@code When.java}</a> based API</i>
 * <p>
 * <p>
 * This class demonstrates how to use Mockito to set up a mock of an asynchronous API as a {@code org.junit.TestRule},
 * to enable using the mock to simulate asynchronous responses from API calls.
 * This makes it possible to write simple unit tests for clients of that API.
 * <p>
 * Calls to {@code resolveNext} or {@code rejectNext} will check a queue for the presence of {@code Deferred} to resolve
 * or reject. To avoid waiting indefinitely in case of failures, there is a timeout specified by the system property {@code com.phenixidentity.junit.runners.timeout}
 * (default value 10 seconds). This timeout will cause the waiting thread to immediately check the queue for any waiting items, and throw an exception if none is found.
 */
public class TestResource extends ExternalResource {
    private static final long TIMEOUT = Long.parseLong(System.getProperty("com.phenixidentity.junit.runners.timeout", "10000"));

    private final PromiseQueue<String> queue;
    private final EventBus eventBus;

    public TestResource(boolean async) {
        When when = async ? WhenFactory.createAsync() : WhenFactory.createSync();
        this.queue = new PromiseQueue<>();
        this.eventBus = mock(EventBus.class);
        when(eventBus.send(anyString()))
                .thenAnswer(invocation -> queue.enqueue(when.defer()));
    }

    public EventBus getMock() {
        return eventBus;
    }

    public <T> void resolveNext(String value) throws InterruptedException {
        if (TIMEOUT > 0) {
            queue.resolveNext(value, TIMEOUT);
        } else {
            queue.resolveNext(value);
        }
    }

    public void rejectNext(Throwable throwable) throws InterruptedException {
        if (TIMEOUT > 0) {
            queue.rejectNext(throwable, TIMEOUT);
        } else {
            queue.rejectNext(throwable);
        }
    }
}
