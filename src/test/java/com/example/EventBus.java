package com.example;

import com.englishtown.promises.Promise;

/**
 * <i>This class is part of a demonstration of how to set up unit tests for a test subject that is using an asynchronous, <a target="_blank" href="https://github.com/ef-labs/when.java">{@code When.java}</a> based API</i>
 * <p>
 * <p>
 * This represents an interface to an imagined asynchronous service, perhaps an event bus that supports sending
 * string messages and receiving string responses back at some later time. The only reason for this interface in
 * the demo is to create a mock, using Mockito, so no implementation is needed here.
 */
public interface EventBus {
    Promise<String> send(String message);
}
